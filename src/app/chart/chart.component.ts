import { Component, OnInit } from '@angular/core';
import { SignalRService } from '../service/signal-r.service';
import { HttpClient } from '@aspnet/signalr';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  constructor(
    public signalRService: SignalRService,
    private http: HttpClient) { }

  ngOnInit() {
    this.signalRService.startConnection();
    this.signalRService.addTransferChartDataListener();
    this.startHttpRequest();
  }

  private startHttpRequest = () => {
  }
}
