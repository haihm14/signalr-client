import { Component, NgZone, OnInit } from '@angular/core';
import { Message } from './models/message';
import { ChatService } from './service/chat-service.service';
import * as signalR from '@aspnet/signalr';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { IRetryPolicy } from './models/Recpmmect';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ClientApp';
  // tslint:disable-next-line: ban-types
  txtMessage: any = '';
  uniqueID: string = new Date().getTime().toString();
  messages = new Array<Message>();
  message = new Message();
  mess: any;
  number1 = 1;
  number2 = 2;
  // tslint:disable-next-line: variable-name
  public hubConnecton: HubConnection;
  constructor() {
  }

  ngOnInit(): void {
    // this.hubConnection = new signalR.HubConnectionBuilder()
    // .withUrl('http://localhost:5000/chathub')
    // .build();

    // this.hubConnection
    // .start()
    // .then(() => console.log('Connection started'))
    // .catch(err => console.log('Error while starting connection: ' + err) );

    this.hubConnecton = new HubConnectionBuilder()
      .withUrl('http://localhost:5000/chathub')
      .withAutomaticReconnect([0, 10000, 20000, null])
      .build();

    this.hubConnecton.on('send', (data) => {
      console.log('Receive: ', data);
    });

    this.hubConnecton.on('all-client', (data) => {
      console.log('Total Client: ', data);
    });

    this.hubConnecton.start()
      .then(() => console.log('Connected!'));
  }

  sendMessage() {
    console.log(this.mess);
    this.hubConnecton.invoke('NewMessage', 'hai', this.mess);
  }
}
